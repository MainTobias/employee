INSERT INTO salary (employment, annual_salary) VALUES ('Junior Developer', 60000);
INSERT INTO salary (employment, annual_salary) VALUES ('Senior Developer', 80000);
INSERT INTO salary (employment, annual_salary) VALUES ('Project Manager', 90000);
INSERT INTO salary (employment, annual_salary) VALUES ('Data Analyst', 75000);
INSERT INTO salary (employment, annual_salary) VALUES ('UX/UI Designer', 70000);

INSERT INTO employee (name, email, salary_id) VALUES ('Franz', 'franz.reichel@htlstp.ac.at', 2);
INSERT INTO employee (name, email, salary_id) VALUES ('Werner', 'werner.gitschthaler@htlstp.ac.at', 2);
INSERT INTO employee (name, email, salary_id) VALUES ('Christoph', 'christoph.schreiber@htlstp.ac.at', 3);
INSERT INTO employee (name, email, salary_id) VALUES ('Sigfried', 'sigfried.schweigl@htlstp.ac.at', 4);
INSERT INTO employee (name, email, salary_id) VALUES ('Evelyn', 'evelyn.kern@htlstp.ac.at', 2);
INSERT INTO employee (name, email, salary_id) VALUES ('Franz', 'franz.mauss@htlstp.ac.at', 2);








