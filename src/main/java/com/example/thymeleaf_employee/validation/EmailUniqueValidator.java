package com.example.thymeleaf_employee.validation;

import com.example.thymeleaf_employee.services.EmployeeService;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class EmailUniqueValidator implements
        ConstraintValidator<EmailUniqueConstraint, String> {
    @Autowired
    private EmployeeService employeeService;

    @Override
    public void initialize(EmailUniqueConstraint emailUnique) {
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext cxt) {
        return !employeeService.isEmailExisting(email);
    }
}