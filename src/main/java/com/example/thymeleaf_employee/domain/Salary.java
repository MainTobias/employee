package com.example.thymeleaf_employee.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@Entity
@ToString
@Table(name = "salary")
public class Salary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotBlank
    @NotEmpty
    @NotNull
    private String employment;

    @PositiveOrZero
    @NotNull
    private Long annualSalary;

    @OneToMany(mappedBy = "salary")
    @ToString.Exclude
    private List<Employee> employees;

}