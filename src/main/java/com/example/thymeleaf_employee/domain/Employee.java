package com.example.thymeleaf_employee.domain;

import com.example.thymeleaf_employee.validation.EmailUniqueConstraint;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(nullable = false)
    @NotBlank
    @NotEmpty
    @NotNull
    private String name;

    @Email
    @EmailUniqueConstraint
    @NotNull
    @Column(nullable = false, unique = true)
    private String email;

    @ManyToOne(optional = false)
    private Salary salary;
}