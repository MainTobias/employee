package com.example.thymeleaf_employee.services;

import com.example.thymeleaf_employee.domain.Salary;
import com.example.thymeleaf_employee.repos.EmployeeRepository;
import com.example.thymeleaf_employee.repos.SalaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalaryService implements ISalaryService {
    private final EmployeeRepository employeeRepository;
    private final SalaryRepository salaryRepository;

    @Autowired
    public SalaryService(EmployeeRepository employeeRepository, SalaryRepository salaryRepository) {
        this.employeeRepository = employeeRepository;
        this.salaryRepository = salaryRepository;
    }


    @Override
    public List<Salary> getAllSalaries() {
        return salaryRepository.findAll();
    }

    @Override
    public Salary getSalaryById(Long id) {
        return salaryRepository.findById(id).get();
    }
}
