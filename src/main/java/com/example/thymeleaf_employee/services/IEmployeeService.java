package com.example.thymeleaf_employee.services;

import com.example.thymeleaf_employee.domain.Employee;

import java.util.List;

public interface IEmployeeService {
    List<Employee> getAllEmployee();
    void save(Employee employee);
    Employee getById(Long id);
    void deleteViaId(Long id);
    boolean isEmailExisting(String email);
}
