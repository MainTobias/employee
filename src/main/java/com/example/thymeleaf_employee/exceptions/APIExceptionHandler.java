package com.example.thymeleaf_employee.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class APIExceptionHandler {
    @ExceptionHandler({RuntimeException.class})
    private ResponseEntity<ProblemDetail> handleAll(RuntimeException re) {
        ProblemDetail pd = ProblemDetail.forStatusAndDetail(
                HttpStatus.BAD_REQUEST,
                re.getMessage()
        );
        System.out.println(re);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(pd);
    }
}
